//
//  ViewController.swift
//  gradient
//
//  Created by Hackintosh on 2/1/19.
//  Copyright © 2019 Hackintosh. All rights reserved.
//

import UIKit

extension UIColor {
    var redValue: CGFloat{ return CIColor(color: self).red }
    var greenValue: CGFloat{ return CIColor(color: self).green }
    var blueValue: CGFloat{ return CIColor(color: self).blue }
    var alphaValue: CGFloat{ return CIColor(color: self).alpha }
}

class ViewController: UIViewController {
    
    let color1: UIColor = UIColor(red: 1, green: 0, blue: 0, alpha: 1)
    let color2: UIColor = UIColor(red: 0, green: 1, blue: 0, alpha: 1)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let mainView = UIView()
        mainView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainView)
        
        mainView.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: 1).isActive = true
        mainView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 1).isActive = true
        mainView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        mainView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        mainView.layoutIfNeeded()
        
        for index in 0..<Int(mainView.frame.width) {
            let color = makeColor(color1: color1, color2: color2, width: mainView.frame.width, index: index)
            makePixel(view: mainView, leadingConstraint: CGFloat(index), color: color)
        }
    }

    func makePixel(view: UIView, /*topConstraint: CGFloat,*/ leadingConstraint: CGFloat, color: UIColor) {
        let pixelView = UIView()
        pixelView.translatesAutoresizingMaskIntoConstraints = false
        pixelView.backgroundColor = color
        view.addSubview(pixelView)
        
        pixelView.widthAnchor.constraint(equalToConstant: 1).isActive = true
        pixelView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 1).isActive = true
        pixelView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        pixelView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: leadingConstraint).isActive = true
    }
    
    func makeColor(color1: UIColor, color2: UIColor, width: CGFloat, index: Int) -> UIColor {
        let diffRed = color1.redValue - color2.redValue
        let diffGreen = color1.greenValue - color2.greenValue
        let diffBlue = color1.blueValue - color2.blueValue
        let color = UIColor(red: makeColorComponent(diff: diffRed, width: width, index: index), green: makeColorComponent(diff: diffGreen, width: width, index: index), blue: makeColorComponent(diff: diffBlue, width: width, index: index), alpha: 1)
        print(color.redValue)
        print(color.greenValue)
        print(color.blueValue)
        return color
    }
    
    func makeColorComponent(diff: CGFloat, width: CGFloat, index: Int) -> CGFloat {
        if diff >= 0 {
            return diff / width * CGFloat(index)
        }
        else {
            return -diff / width * (width - CGFloat(index))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

